package com.example.czaja.myapplication.fragment;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.MainThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.example.czaja.myapplication.GraphPanel;
import com.example.czaja.myapplication.R;
import com.example.czaja.myapplication.datebase.Baza;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class GraphFragment extends Fragment {

    public GraphFragment() {
        // Required empty public constructor
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graph, container, false);
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        String type = bundle.getString("type");
        ArrayList<Integer> listMatrixOderBy = new ArrayList<Integer>();
        String algorytm = bundle.getString("algorytm");

        if(type.equals("sasiedztwa"))
        {
            ArrayList<Integer> listMatrix = bundle.getIntegerArrayList("matrix");

            for (int i = listMatrix.size() - 1; i >= 0; i--)
            {
                listMatrixOderBy.add(listMatrix.get(i));
                Log.d("bundle: ", String.valueOf(listMatrix.get(i)));
            }
        }

        if(type.equals("incydencji") || type.equals("lista") || type.equals("edit"))
        {
            listMatrixOderBy = bundle.getIntegerArrayList("matrix");
        }

        String strMatrix = "";

        for(Integer number : listMatrixOderBy)
        {
            strMatrix += number + ",";
        }

        Baza baza = new Baza(getActivity());
        baza.insertTraining(strMatrix);

        //listMatrixOderBy = bundle.getIntegerArrayList("matrix");
        // Inflate the layout for this fragment
        return new GraphPanel(getActivity(), listMatrixOderBy, algorytm);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Fragment fragment = new SasiedztwaFragment();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.add(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
