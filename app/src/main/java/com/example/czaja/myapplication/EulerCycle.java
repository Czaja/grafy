package com.example.czaja.myapplication;

import java.util.ArrayList;

/**
 * Created by Czaja on 30.01.2018.
 */

public class EulerCycle {

    private int vertices;
    private ArrayList<Integer>[] adj;

    EulerCycle(int numOfVertices)
    {
        this.vertices = numOfVertices;

        initGraph();
    }

    @SuppressWarnings("unchecked")
    private void initGraph()
    {
        adj = new ArrayList[vertices];
        for (int i = 0; i < vertices; i++)
        {
            adj[i] = new ArrayList<>();
        }
    }

    public void addEdge(Integer u, Integer v)
    {
        adj[u].add(v);
        adj[v].add(u);
    }

    private void removeEdge(Integer u, Integer v)
    {
        adj[u].remove(v);
        adj[v].remove(u);
    }

    public String printEulerTour()
    {
        String strEuler = "";
        Integer u = 0;
        for (int i = 0; i < vertices; i++)
        {
            if (adj[i].size() % 2 == 1)
            {
                u = i;
                break;
            }
        }

        strEuler = printEulerUtil(u, strEuler);
        if (strEuler != "")
        {
            strEuler = strEuler.substring(0, strEuler.length() - 1);
        }
        return strEuler;
    }

    private String printEulerUtil(Integer u, String strEuler)
    {
        for (int i = 0; i < adj[u].size(); i++)
        {
            Integer v = adj[u].get(i);
            if (isValidNextEdge(u, v))
            {
                strEuler += u + "-" + v + ",";

                removeEdge(u, v);
                strEuler = printEulerUtil(v, strEuler);
            }
        }

        return strEuler;
    }

    private boolean isValidNextEdge(Integer u, Integer v)
    {
        if (adj[u].size() == 1) {
            return true;
        }

        boolean[] isVisited = new boolean[this.vertices];
        int count1 = dfsCount(u, isVisited);

        removeEdge(u, v);
        isVisited = new boolean[this.vertices];
        int count2 = dfsCount(u, isVisited);

        addEdge(u, v);
        return (count1 > count2) ? false : true;
    }

    private int dfsCount(Integer v, boolean[] isVisited)
    {
        isVisited[v] = true;
        int count = 1;

        for (int adj : adj[v])
        {
            if (!isVisited[adj])
            {
                count = count + dfsCount(adj, isVisited);
            }
        }
        return count;
    }

}
