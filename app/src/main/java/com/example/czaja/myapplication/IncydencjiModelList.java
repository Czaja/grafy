package com.example.czaja.myapplication;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Czaja on 15.11.2017.
 */

public class IncydencjiModelList {
    private ArrayList<IncydencjiModel> listIncydencjiModel;

    public IncydencjiModelList()
    {
        listIncydencjiModel = new ArrayList<IncydencjiModel>();
    }

    public void AddIncydencjiModel(IncydencjiModel incydencjiModel)
    {
        listIncydencjiModel.add(incydencjiModel);
    }

    public ArrayList<IncydencjiModel> GetAllIncydencjiModel()
    {
        return listIncydencjiModel;
    };

    public void removeAll()
    {
        listIncydencjiModel = new ArrayList<IncydencjiModel>();
    }

    public IncydencjiModel getByVertexEdge(int vertex, int edge)
    {
        for (int i = 0; i < GetAllIncydencjiModel().size(); i++)
        {
            if (GetAllIncydencjiModel().get(i).getVertex() == vertex &&GetAllIncydencjiModel().get(i).getEdge() == edge)
            {
                return GetAllIncydencjiModel().get(i);
            }
        }

        return null;
    }

    public ArrayList<IncydencjiModel> getByEdge(int edge)
    {
        ArrayList<IncydencjiModel> localListIncydencjiModel = new ArrayList<IncydencjiModel>();

        for (int i = 0; i < GetAllIncydencjiModel().size(); i++)
        {
            if (GetAllIncydencjiModel().get(i).getEdge() == edge && GetAllIncydencjiModel().get(i).getEditText().getText().toString().equals("1"))
            {
                localListIncydencjiModel.add(GetAllIncydencjiModel().get(i));
            }
        }

        return localListIncydencjiModel;
    }
}
