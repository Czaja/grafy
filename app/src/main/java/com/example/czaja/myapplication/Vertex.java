package com.example.czaja.myapplication;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;

import java.util.ArrayList;

/**
 * Created by Czaja on 26.10.2017.
 */

public class Vertex implements IVertex {

    private RectF rectangle;
    private int color;
    public static final int DIAMETER = 100;
    public int ordinal;
    private EdgesList listEdges;
    private EdgesList listEdgesHamilton;
    private String textVisible;

    public Vertex(int x, int y, int color, int ordinal, EdgesList listEdges, EdgesList listEdgesHamilton) {
        this.rectangle = new RectF(x, y, x + DIAMETER, y + DIAMETER);
        this.color = color;
        this.ordinal = ordinal;
        this.listEdges = listEdges;
        this.listEdgesHamilton = listEdgesHamilton;
    }


    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawOval(rectangle, paint);
        Paint paintText = new Paint();
        paint.setTextSize(60);
        paint.setColor(Color.WHITE);
        canvas.drawText(Integer.toString(ordinal), centerPixel().x - 15, centerPixel().y + 20, paint);

        if (textVisible != null) {
            paint.setColor(Color.BLACK);

            paint.setTextSize(60);
            canvas.drawText(getTextVisible(), 10, 1300, paint);
        }

        for (Edges edges : listEdges.GetAllEdges())
        {
            if (edges.getStartVertex() == ordinal)
            {
                edges.setStart(new Point(centerPixel().x, centerPixel().y));
            }
        }

        for (Edges edges : listEdges.GetAllEdges())
        {
            if (edges.getStopVertex() == ordinal)
            {
                edges.setEnd(new Point(centerPixel().x, centerPixel().y));
            }
        }
    }

    @Override
    public Point centerPixel() {
        return new Point((int)((rectangle.right + rectangle.left) / 2),
                (int)((rectangle.bottom + rectangle.top) / 2));
    }

    @Override
    public void update() {

    }

    @Override
    public void update(Point point) {
        rectangle.set(point.x - rectangle.width() / 2, point.y - rectangle.height() / 2,
                point.x + rectangle.width() / 2, point.y + rectangle.height() / 2);
    }

    public RectF getRectangle() {
        return rectangle;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public String getTextVisible() {
        return textVisible;
    }

    public void setTextVisible(String textVisible) {
        this.textVisible = textVisible;
    }
}
