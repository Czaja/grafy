package com.example.czaja.myapplication.fragment;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.czaja.myapplication.IncydencjiModel;
import com.example.czaja.myapplication.ListModel;
import com.example.czaja.myapplication.ListModelList;
import com.example.czaja.myapplication.R;

import java.text.Format;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaFragment extends Fragment {

    private GridLayout gridLayoutMatrix;
    private ArrayList<EditText> listEditText;
    private Button buttonDrawGraph;
    private ListModelList listModelList;
    private Spinner spinnerMatrixSize;
    private ArrayList<Integer> listMatrix;
    private Button buttonDrawGraphHamilton;
    private Button buttonDrawGraphEulera;
    private Button buttonDrawGraphBFS;
    private Button buttonDrawGraphColoring;
    private Button buttonDrawGraphMost;

    public ListaFragment()
    {

    }

    public ListaFragment(ArrayList<Integer> listMatrix) {
        this.listMatrix = listMatrix;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista, container, false);
        spinnerMatrixSize = (Spinner) view.findViewById(R.id.spinner_matrix_size);
        gridLayoutMatrix = (GridLayout) view.findViewById(R.id.draw_matrix);
        buttonDrawGraph = (Button) view.findViewById(R.id.draw_graph);
        listEditText = new ArrayList<EditText>();
        listModelList = new ListModelList();
        spinnerMatrixSize = (Spinner) view.findViewById(R.id.spinner_matrix_size);
        buttonDrawGraphHamilton = (Button) view.findViewById(R.id.draw_graph_hamilton);
        buttonDrawGraphEulera = (Button) view.findViewById(R.id.draw_graph_eulera);
        buttonDrawGraphBFS = (Button) view.findViewById(R.id.draw_graph_bfs);
        buttonDrawGraphMost = (Button) view.findViewById(R.id.draw_graph_most);
        buttonDrawGraphColoring = (Button) view.findViewById(R.id.draw_graph_coloring);

        BindSpinnerMatrixSize(view);
        BindButtonDrawGraph();

        return view;
    }

    private void BindButtonDrawGraph()
    {
        buttonDrawGraph.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int vertices = Integer.parseInt(spinnerMatrixSize.getSelectedItem().toString());

                int tab[][] = new int[vertices][vertices];
                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        tab[i][j] = 0;
                    }
                }
                for (int i = 0; i < 8; i++)
                {
                    ArrayList<ListModel> localListListModel = listModelList.getByVertex(i);

                    for (ListModel listModel : localListListModel)
                    {
                        tab[i][Integer.parseInt(listModel.getEditText().getText().toString())] = 1;
                    }
                }

                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        listMatrix.add(tab[i][j]);
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "lista");
                bundle.putString("algorytm", "");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphHamilton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int vertices = Integer.parseInt(spinnerMatrixSize.getSelectedItem().toString());

                int tab[][] = new int[vertices][vertices];
                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        tab[i][j] = 0;
                    }
                }
                for (int i = 0; i < 8; i++)
                {
                    ArrayList<ListModel> localListListModel = listModelList.getByVertex(i);

                    for (ListModel listModel : localListListModel)
                    {
                        tab[i][Integer.parseInt(listModel.getEditText().getText().toString())] = 1;
                    }
                }

                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        listMatrix.add(tab[i][j]);
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "lista");
                bundle.putString("algorytm", "cykl_hamiltona");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphEulera.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int vertices = Integer.parseInt(spinnerMatrixSize.getSelectedItem().toString());

                int tab[][] = new int[vertices][vertices];
                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        tab[i][j] = 0;
                    }
                }
                for (int i = 0; i < 8; i++)
                {
                    ArrayList<ListModel> localListListModel = listModelList.getByVertex(i);

                    for (ListModel listModel : localListListModel)
                    {
                        tab[i][Integer.parseInt(listModel.getEditText().getText().toString())] = 1;
                    }
                }

                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        listMatrix.add(tab[i][j]);
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "lista");
                bundle.putString("algorytm", "cykl_eulera");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphMost.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int vertices = Integer.parseInt(spinnerMatrixSize.getSelectedItem().toString());

                int tab[][] = new int[vertices][vertices];
                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        tab[i][j] = 0;
                    }
                }
                for (int i = 0; i < 8; i++)
                {
                    ArrayList<ListModel> localListListModel = listModelList.getByVertex(i);

                    for (ListModel listModel : localListListModel)
                    {
                        tab[i][Integer.parseInt(listModel.getEditText().getText().toString())] = 1;
                    }
                }

                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        listMatrix.add(tab[i][j]);
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "lista");
                bundle.putString("algorytm", "cykl_most");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphColoring.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int vertices = Integer.parseInt(spinnerMatrixSize.getSelectedItem().toString());

                int tab[][] = new int[vertices][vertices];
                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        tab[i][j] = 0;
                    }
                }
                for (int i = 0; i < 8; i++)
                {
                    ArrayList<ListModel> localListListModel = listModelList.getByVertex(i);

                    for (ListModel listModel : localListListModel)
                    {
                        tab[i][Integer.parseInt(listModel.getEditText().getText().toString())] = 1;
                    }
                }

                for (int i = 0; i < vertices; i++)
                {
                    for (int j = 0; j < vertices; j++)
                    {
                        listMatrix.add(tab[i][j]);
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "lista");
                bundle.putString("algorytm", "cykl_koloruj");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });
    }

    private void BindSpinnerMatrixSize(View view) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.matrix_count, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);

        spinnerMatrixSize.setAdapter(adapter);

        spinnerMatrixSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                GenerateMatrix(parentView.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    private void GenerateMatrix(String selectSizeMatrix)
    {
        gridLayoutMatrix.removeAllViews();
        listEditText = new ArrayList<EditText>();
        listModelList.removeAll();

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(1);
        EditText editTextMatrix = null;
        int sizeMatrix = Integer.parseInt(selectSizeMatrix.substring(0, 1));
        gridLayoutMatrix.setColumnCount(sizeMatrix);
        gridLayoutMatrix.setRowCount(sizeMatrix);

        int k = 0;
        for (int i = 0; i < sizeMatrix; i++)
        {
            for (int j = 0; j < sizeMatrix; j++)
            {
                editTextMatrix = new EditText(this.getActivity());
                editTextMatrix.setFilters(filterArray);
                editTextMatrix.setText("-");

                gridLayoutMatrix.addView(editTextMatrix, k);
                listModelList.AddListModel(new ListModel(editTextMatrix, i));
                k++;
            }
        }
    }
}
