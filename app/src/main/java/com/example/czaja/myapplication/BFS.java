package com.example.czaja.myapplication;

/**
 * Created by Czaja on 31.01.2018.
 */

import java.util.Iterator;
import java.util.LinkedList;

public class BFS {
    private int V;   // No. of vertices
    private LinkedList<Integer> adj[]; //Adjacency Lists

    // Constructor
    public BFS(int v)
    {
        V = v;
        adj = new LinkedList[v];
        for (int i=0; i<v; ++i)
            adj[i] = new LinkedList();
    }

    public void addEdge(int v,int w)
    {
        adj[v].add(w);
    }

    public String BFS(int s)
    {
        boolean visited[] = new boolean[V];
        String bfs = "";

        LinkedList<Integer> queue = new LinkedList<Integer>();

        visited[s]=true;
        queue.add(s);

        while (queue.size() != 0)
        {
            s = queue.poll();
            bfs += s + ",";

            Iterator<Integer> i = adj[s].listIterator();
            while (i.hasNext())
            {
                int n = i.next();
                if (!visited[n])
                {
                    visited[n] = true;
                    queue.add(n);
                }
            }
        }

        return bfs;
    }
}
