package com.example.czaja.myapplication.datebase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Czaja on 18.11.2017.
 */

public class Baza extends SQLiteOpenHelper {

    private static final String DB_NAME = "matrix";
    private static final int DB_VERSION = 1;
    public static final String TABLE_MATRIX = "column_matrix";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_MATRIX = "column_matrix";

    private static final String CREATE_DATABASE_MATRIX = "CREATE TABLE " + TABLE_MATRIX + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_MATRIX + " TEXT "
            + ");";

    public Baza(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DATABASE_MATRIX);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion, newVersion);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void insertTraining(String matrix) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues trainingValues = new ContentValues();
        trainingValues.put(COLUMN_MATRIX, matrix);
        db.insert(TABLE_MATRIX, null, trainingValues);
    }

    public String getLastMatrix()
    {
        String[] kolumny = {COLUMN_MATRIX};
        SQLiteDatabase db = getReadableDatabase();

        Cursor kursor = db.query(TABLE_MATRIX, kolumny, null, null, null, null, null);

        kursor.moveToLast();

        return kursor.getString(0);
    }
}
