package com.example.czaja.myapplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Czaja on 27.10.2017.
 */

public class VertexList {
    private ArrayList<VertexPoint> listVertexPoints;

    public VertexList()
    {
        listVertexPoints = new ArrayList<VertexPoint>();
    }

    public void AddVertexPoint(VertexPoint vertexPoint)
    {
        listVertexPoints.add(vertexPoint);
    }

    public ArrayList<VertexPoint> GetAllVertexPoint()
    {
        return listVertexPoints;
    }
}
