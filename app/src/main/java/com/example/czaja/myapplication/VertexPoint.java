package com.example.czaja.myapplication;

import android.graphics.Point;

/**
 * Created by Czaja on 27.10.2017.
 */

public class VertexPoint {
    private Vertex vertex;
    private Point point;

    public VertexPoint(Vertex vertexPoint, Point point)
    {
        this.vertex = vertexPoint;
        this.point = point;
    }

    public Vertex getVertex() {
        return vertex;
    }

    public Point getPoint() {
        return point;
    }

    public void setVertex(Vertex vertex) {
        this.vertex = vertex;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
