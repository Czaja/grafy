package com.example.czaja.myapplication;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

/**
 * Created by Czaja on 28.10.2017.
 */

public class Edges {
    private Point start;
    private Point end;
    private int startVertex;
    private int stopVertex;
    private int count;
    private int colorVertex;

    public Edges(Point start, Point end, int startVertex, int stopVertex, int count, int colorVertex)
    {
        this.start = start;
        this.end = end;
        this.startVertex = startVertex;
        this.stopVertex = stopVertex;
        this.count = count;
        this.colorVertex = colorVertex;
    }

    public void draw(Canvas canvas)
    {
        Paint paintLine = new Paint();
        paintLine.setStyle(Paint.Style.STROKE);
        paintLine.setStrokeWidth(8);
        paintLine.setColor(colorVertex);
        canvas.drawLine(getStart().x, getStart().y, getEnd().x, getEnd().y, paintLine);
    }

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public int getStartVertex() {
        return startVertex;
    }

    public int getStopVertex() {
        return stopVertex;
    }

    public int getCount() {
        return count;
    }
}
