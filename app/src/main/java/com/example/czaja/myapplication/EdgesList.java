package com.example.czaja.myapplication;

import java.util.ArrayList;

/**
 * Created by Czaja on 28.10.2017.
 */

public class EdgesList {
    private ArrayList<Edges> listEdges;

    public EdgesList()
    {
        listEdges = new ArrayList<Edges>();
    }

    public void AddEdges(Edges edges)
    {
        listEdges.add(edges);
    }

    public ArrayList<Edges> GetAllEdges()
    {
        return listEdges;
    }
}
