package com.example.czaja.myapplication;

/**
 * Created by Czaja on 31.01.2018.
 */

import java.util.Iterator;
import java.util.LinkedList;

public class Most {

    private int V;

    private LinkedList<Integer> adj[];
    int time = 0;
    static final int NIL = -1;

    public Most(int v)
    {
        V = v;
        adj = new LinkedList[v];
        for (int i=0; i<v; ++i)
            adj[i] = new LinkedList();
    }

    public void addEdge(int v, int w)
    {
        adj[v].add(w);
        adj[w].add(v);
    }

    public String bridgeUtil(int u, boolean visited[], int disc[],
                    int low[], int parent[], String most)
    {
        int children = 0;

        visited[u] = true;

        disc[u] = low[u] = ++time;

        Iterator<Integer> i = adj[u].iterator();
        while (i.hasNext())
        {
            int v = i.next();  // v is current adjacent of u

            if (!visited[v])
            {
                parent[v] = u;
                most = bridgeUtil(v, visited, disc, low, parent, most);

                low[u]  = Math.min(low[u], low[v]);

                if (low[v] > disc[u])
                    most += u + "-" + v + ",";
            }

            else if (v != parent[u])
                low[u]  = Math.min(low[u], disc[v]);
        }

        return most;
    }

    public String bridge()
    {
        boolean visited[] = new boolean[V];
        int disc[] = new int[V];
        int low[] = new int[V];
        int parent[] = new int[V];
        String most = "";

        for (int i = 0; i < V; i++)
        {
            parent[i] = NIL;
            visited[i] = false;
        }

        for (int i = 0; i < V; i++)
            if (visited[i] == false)
                most += bridgeUtil(i, visited, disc, low, parent, most);

        if (most != "")
        {
            most = most.substring(0, most.length() - 1);
        }

        return most;
    }
}
