package com.example.czaja.myapplication;

import android.widget.EditText;

/**
 * Created by Czaja on 15.11.2017.
 */

public class IncydencjiModel {
    private EditText editText;
    private int vertex;
    private int edge;

    public IncydencjiModel(EditText editText, int vertex, int edge) {
        this.editText = editText;
        this.vertex = vertex;
        this.edge = edge;
    }

    public EditText getEditText() {
        return editText;
    }

    public int getVertex() {
        return vertex;
    }

    public int getEdge() {
        return edge;
    }
}
