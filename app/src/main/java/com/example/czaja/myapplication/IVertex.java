package com.example.czaja.myapplication;

import android.graphics.Canvas;
import android.graphics.Point;

/**
 * Created by Czaja on 26.10.2017.
 */

public interface IVertex {
    public void draw(Canvas canvas);
    public Point centerPixel();
    public void update();
    public void update(Point point);
}
