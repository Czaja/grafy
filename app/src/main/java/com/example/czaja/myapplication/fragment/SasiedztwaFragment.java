package com.example.czaja.myapplication.fragment;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.czaja.myapplication.R;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SasiedztwaFragment extends Fragment {

    private Spinner spinnerMatrixSize;
    private GridLayout gridLayoutMatrix;
    private ArrayList<EditText> listEditText;
    private Button buttonDrawGraph;
    private Button buttonDrawGraphHamilton;
    private Button buttonDrawGraphEulera;
    private Button buttonDrawGraphBFS;
    private Button buttonDrawGraphMost;
    private Button buttonDrawGraphColoring;
    private ArrayList<Integer> listMatrix;

    public SasiedztwaFragment() {
        // Required empty public constructor
    }

    public SasiedztwaFragment(ArrayList<Integer> listMatrix) {
        this.listMatrix = listMatrix;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sasiedztwa, container, false);
        spinnerMatrixSize = (Spinner) view.findViewById(R.id.spinner_matrix_size);
        gridLayoutMatrix = (GridLayout) view.findViewById(R.id.draw_matrix);
        buttonDrawGraph = (Button) view.findViewById(R.id.draw_graph);
        buttonDrawGraphHamilton = (Button) view.findViewById(R.id.draw_graph_hamilton);
        buttonDrawGraphEulera = (Button) view.findViewById(R.id.draw_graph_eulera);
        buttonDrawGraphBFS = (Button) view.findViewById(R.id.draw_graph_bfs);
        buttonDrawGraphMost = (Button) view.findViewById(R.id.draw_graph_most);
        buttonDrawGraphColoring = (Button) view.findViewById(R.id.draw_graph_coloring);
        listEditText = new ArrayList<EditText>();

        BindSpinnerMatrixSize(view);
        BindButtonDrawGraph();

        return view;
    }

    private void BindButtonDrawGraph()
    {
        buttonDrawGraphHamilton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "sasiedztwa");
                bundle.putString("algorytm", "cykl_hamiltona");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraph.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "sasiedztwa");
                bundle.putString("algorytm", "");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphEulera.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "sasiedztwa");
                bundle.putString("algorytm", "cykl_eulera");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphBFS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "sasiedztwa");
                bundle.putString("algorytm", "cykl_bfs");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphMost.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "sasiedztwa");
                bundle.putString("algorytm", "cykl_most");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphColoring.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "sasiedztwa");
                bundle.putString("algorytm", "cykl_koloruj");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });
    }

    private void BindSpinnerMatrixSize(View view) {
        Spinner spinnerMatrixSize = (Spinner) view.findViewById(R.id.spinner_matrix_size);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.matrix_size, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);

        spinnerMatrixSize.setAdapter(adapter);

        spinnerMatrixSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                GenerateMatrix(parentView.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    private void GenerateMatrix(String selectSizeMatrix)
    {
        gridLayoutMatrix.removeAllViews();
        listEditText = new ArrayList<EditText>();

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(1);
        EditText editTextMatrix = null;
        int sizeMatrix = Integer.parseInt(selectSizeMatrix.substring(0, 1));
        gridLayoutMatrix.setColumnCount(sizeMatrix);
        gridLayoutMatrix.setRowCount(sizeMatrix);

        for (int i = 0; i < Math.pow(sizeMatrix, 2); i++)
        {
            editTextMatrix = new EditText(this.getActivity());
            editTextMatrix.setFilters(filterArray);
            editTextMatrix.setText("0");

            gridLayoutMatrix.addView(editTextMatrix, 0);
            listEditText.add(editTextMatrix);
        }

    }
}
