package com.example.czaja.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.czaja.myapplication.fragment.Coloring;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Czaja on 26.10.2017.
 */

public class GraphPanel extends SurfaceView implements SurfaceHolder.Callback {

    private MainThread thread;
    private VertexList vertexList;
    private EdgesList listEdges;
    private EdgesList listEdgesHamilton;
    private HamiltonianCycle hamiltonianCycle;
    private String textVisible;

    public GraphPanel(Context context, ArrayList<Integer> listMatrix, String algorytm) {
        super(context);

        vertexList = new VertexList();
        listEdges = new EdgesList();
        listEdgesHamilton = new EdgesList();

        getHolder().addCallback(this);

        thread = new MainThread(getHolder(), this);

        createEdges(listMatrix);
        if (algorytm.equals("cykl_koloruj"))
        {
            drawColorOval(listMatrix);
        }
        else
        {
            drawOval(listMatrix);
        }

        if (algorytm.equals("cykl_hamiltona"))
        {
            createEdgesHamilton(listMatrix);
        }
        if (algorytm.equals("cykl_eulera"))
        {
            createEdgesEulera(listMatrix);
        }
        if (algorytm.equals("cykl_bfs"))
        {
            createEdgesBFS(listMatrix);
        }
        if (algorytm.equals("cykl_most"))
        {
            createEdgesMost(listMatrix);
        }

        setFocusable(true);
    }

    public void createEdges(ArrayList<Integer> listMatrix)
    {
        int countVertex = (int)Math.sqrt(listMatrix.size());

        for(int i = 0, start = 0; i < listMatrix.size(); i++, start++)
        {
            if (start == countVertex)
            {
                start = 0;
            }

            for (int end = 0; end < countVertex && i < listMatrix.size(); end++, i++)
            {
                listEdges.AddEdges(new Edges(new Point(0, 0), new Point(0, 0), start, end, listMatrix.get(i), Color.BLACK));
            }
            i--;
        }
    }

    public void createEdgesMost(ArrayList<Integer> listMatrix)
    {
        int countVertex = (int)Math.sqrt(listMatrix.size());
        int[][] tabCountVertex = new int[countVertex][countVertex];
        Most most = new Most(countVertex);
        ArrayList<Integer> startList = new ArrayList<Integer>();
        ArrayList<Integer> endList = new ArrayList<Integer>();

        for (int i = 0, k = 0; i < countVertex; i++)
        {
            for (int j = 0; j < countVertex; j++)
            {
                tabCountVertex[i][j] = listMatrix.get(k);
                k++;
            }
        }

        for (int i = 0; i < countVertex; i++)
        {
            for (int j = i; j < countVertex; j++)
            {
                if (tabCountVertex[i][j] == 1)
                {
                    most.addEdge(i, j);
                }
            }
        }

        textVisible = most.bridge();
        String[] splitText = textVisible.split(",");

        if (splitText.length != 0)
        {
            for (int i = 0; i < splitText.length; i++)
            {
                String[] splt = splitText[i].split("-");
                startList.add(Integer.parseInt(splt[0]));
                endList.add(Integer.parseInt(splt[1]));
            }
        }
        else
        {
            if (textVisible != "")
            {
                String[] splt = textVisible.split("-");
                startList.add(Integer.parseInt(splt[0]));
                endList.add(Integer.parseInt(splt[1]));
            }
        }


        for(int i = 0, start = 0; i < listMatrix.size(); i++, start++)
        {
            if (start == countVertex)
            {
                start = 0;
            }

            for (int end = 0; end < countVertex && i < listMatrix.size(); end++, i++)
            {
                boolean mostBool = false;
                for (int c = 0; c < startList.size(); c++)
                {
                    if ((startList.get(c) == start && endList.get(c) == end) ||
                            (startList.get(c) == end && endList.get(c) == start))
                    {
                        mostBool = true;
                    }
                }
                if (mostBool)
                {
                    listEdges.AddEdges(new Edges(new Point(0, 0), new Point(0, 0), start, end, listMatrix.get(i), Color.BLACK));
                }
                else
                {
                    listEdges.AddEdges(new Edges(new Point(0, 0), new Point(0, 0), start, end, listMatrix.get(i), Color.RED));
                }
            }
            i--;
        }
    }

    public void createEdgesEulera(ArrayList<Integer> listMatrix)
    {
        int countVertex = (int)Math.sqrt(listMatrix.size());
        int[][] tabCountVertex = new int[countVertex][countVertex];
        EulerCycle eulerCycle = new EulerCycle(countVertex);

        for (int i = 0, k = 0; i < countVertex; i++)
        {
            for (int j = 0; j < countVertex; j++)
            {
                tabCountVertex[i][j] = listMatrix.get(k);
                k++;
            }
        }

        for (int i = 0; i < countVertex; i++)
        {
            for (int j = i; j < countVertex; j++)
            {
                if (tabCountVertex[i][j] == 1)
                {
                    eulerCycle.addEdge(i, j);
                }
            }
        }
        textVisible = eulerCycle.printEulerTour();

        for(int i = 0, start = 0; i < listMatrix.size(); i++, start++)
        {
            if (start == countVertex)
            {
                start = 0;
            }

            for (int end = 0; end < countVertex && i < listMatrix.size(); end++, i++)
            {
                listEdges.AddEdges(new Edges(new Point(0, 0), new Point(0, 0), start, end, listMatrix.get(i), Color.BLACK));
            }
            i--;
        }
    }

    public void createEdgesBFS(ArrayList<Integer> listMatrix)
    {
        int countVertex = (int)Math.sqrt(listMatrix.size());
        int[][] tabCountVertex = new int[countVertex][countVertex];
        BFS graphBFS = new BFS(countVertex);

        for (int i = 0, k = 0; i < countVertex; i++)
        {
            for (int j = 0; j < countVertex; j++)
            {
                tabCountVertex[i][j] = listMatrix.get(k);
                k++;
            }
        }

        for (int i = 0; i < countVertex; i++)
        {
            for (int j = i; j < countVertex; j++)
            {
                if (tabCountVertex[i][j] == 1)
                {
                    graphBFS.addEdge(i, j);
                }
            }
        }
        textVisible = graphBFS.BFS(0);

        for(int i = 0, start = 0; i < listMatrix.size(); i++, start++)
        {
            if (start == countVertex)
            {
                start = 0;
            }

            for (int end = 0; end < countVertex && i < listMatrix.size(); end++, i++)
            {
                listEdges.AddEdges(new Edges(new Point(0, 0), new Point(0, 0), start, end, listMatrix.get(i), Color.BLACK));
            }
            i--;
        }
    }

    public void createEdgesHamilton(ArrayList<Integer> listMatrix)
    {
        int countVertex = (int)Math.sqrt(listMatrix.size());
        int[][] tabCountVertex = new int[countVertex][countVertex];
        ArrayList<Integer> listCycleHamilton = new ArrayList<Integer>();
        ArrayList<String> road = new ArrayList<String>();
        hamiltonianCycle = new HamiltonianCycle(countVertex);

        for (int i = 0, k = 0; i < countVertex; i++)
        {
            for (int j = 0; j < countVertex; j++)
            {
                tabCountVertex[i][j] = listMatrix.get(k);
                k++;
            }
        }

        hamiltonianCycle = HamiltonianCycleVertexName(hamiltonianCycle, countVertex);

        for (int i = 0; i < countVertex; i++)
        {
            String test = "";
            for (int j = 0; j < countVertex; j++)
            {
                test += tabCountVertex[i][j] + " ";
            }
            Log.d("", test);
        }

        String strCycleHamilton = hamiltonianCycle.isHamiltonianCycle(tabCountVertex);

        Log.d("", strCycleHamilton);

        String[] tableCycleHamilton = strCycleHamilton.split(",");

        if (!tableCycleHamilton[0].equals(""))
        {
            for (int i = 0; i < tableCycleHamilton.length ; i++)
            {
                listCycleHamilton.add(Integer.parseInt(tableCycleHamilton[i]));
            }
        }

        for(int i = 0, start = 0; i < listMatrix.size(); i++, start++)
        {
            if (start == countVertex)
            {
                start = 0;
            }

            for (int end = 0; end < countVertex && i < listMatrix.size(); end++, i++)
            {
                boolean hamilton = false;
                for (int c = 0; c < listCycleHamilton.size() - 1; c++)
                {
                    if ((listCycleHamilton.get(c) == start && listCycleHamilton.get(c+1) == end) ||
                            (listCycleHamilton.get(c) == end && listCycleHamilton.get(c+1) == start))
                    {
                        hamilton = true;
                    }
                }
                if (hamilton)
                {
                    listEdges.AddEdges(new Edges(new Point(0, 0), new Point(0, 0), start, end, listMatrix.get(i), Color.BLACK));
                }
                else
                {
                    listEdges.AddEdges(new Edges(new Point(0, 0), new Point(0, 0), start, end, listMatrix.get(i), Color.RED));
                }
            }
            i--;
        }
    }

    public void drawOval(ArrayList<Integer> listMatrix)
    {
        int countVertex = (int)Math.sqrt(listMatrix.size());

        switch (countVertex)
        {
            case 1:
                AddOval(400, 400, 0, "0");
                break;
            case 2:
                AddOval(150, 150, 0, "0");
                AddOval(600, 150, 1, "0");
                break;
            case 3:
                AddOval(150, 150, 0, "0");
                AddOval(600, 150, 1, "0");
                AddOval(375, 600, 2, "0");
                break;
            case 4:
                AddOval(150, 150, 0, "0");
                AddOval(150, 600, 1, "0");
                AddOval(600, 150, 2, "0");
                AddOval(600, 600, 3, "0");
                break;
            case 5:
                AddOval(150, 150, 0, "0");
                AddOval(150, 600, 1, "0");
                AddOval(600, 150, 2, "0");
                AddOval(600, 600, 3, "0");
                AddOval(375, 800, 4, "0");
                break;
            case 6:
                AddOval(150, 150, 0, "0");
                AddOval(600, 150, 1, "0");
                AddOval(300, 450, 2, "0");
                AddOval(450, 450, 3, "0");
                AddOval(150, 800, 4, "0");
                AddOval(600, 800, 5, "0");
                break;
            case 7:
                AddOval(150, 150, 0, "0");
                AddOval(600, 150, 1, "0");
                AddOval(300, 450, 2, "0");
                AddOval(450, 450, 3, "0");
                AddOval(150, 800, 4, "0");
                AddOval(600, 800, 5, "0");
                AddOval(375, 1200, 6, "0");
                break;
            case 8:
                AddOval(150, 150, 0, "0");
                AddOval(600, 150, 1, "0");
                AddOval(300, 450, 2, "0");
                AddOval(450, 450, 3, "0");
                AddOval(150, 800, 4, "0");
                AddOval(600, 800, 5, "0");
                AddOval(300, 1200, 6, "0");
                AddOval(450, 1200, 7, "0");
                break;
        }
    }

    public void drawColorOval(ArrayList<Integer> listMatrix)
    {
        int countVertex = (int)Math.sqrt(listMatrix.size());
        int[][] tabCountVertex = new int[countVertex][countVertex];
        Coloring coloringList = new Coloring(countVertex);

        for (int i = 0, k = 0; i < countVertex; i++)
        {
            for (int j = 0; j < countVertex; j++)
            {
                tabCountVertex[i][j] = listMatrix.get(k);
                k++;
            }
        }

        for (int i = 0; i < countVertex; i++)
        {
            for (int j = i; j < countVertex; j++)
            {
                if (tabCountVertex[i][j] == 1)
                {
                    coloringList.addEdge(i, j);
                }
            }
        }

        String strColoring = coloringList.greedyColoring();

        String[] splitColoring = strColoring.split(",");

        switch (countVertex)
        {
            case 1:
                AddOval(400, 400, 0, "0");
                break;
            case 2:
                AddOval(150, 150, 0, splitColoring[0]);
                AddOval(600, 150, 1, splitColoring[1]);
                break;
            case 3:
                AddOval(150, 150, 0, splitColoring[0]);
                AddOval(600, 150, 1, splitColoring[1]);
                AddOval(375, 600, 2, splitColoring[2]);
                break;
            case 4:
                AddOval(150, 150, 0, splitColoring[0]);
                AddOval(150, 600, 1, splitColoring[1]);
                AddOval(600, 150, 2, splitColoring[2]);
                AddOval(600, 600, 3, splitColoring[3]);
                break;
            case 5:
                AddOval(150, 150, 0, splitColoring[0]);
                AddOval(150, 600, 1, splitColoring[1]);
                AddOval(600, 150, 2, splitColoring[2]);
                AddOval(600, 600, 3, splitColoring[3]);
                AddOval(375, 800, 4, splitColoring[4]);
                break;
            case 6:
                AddOval(150, 150, 0, splitColoring[0]);
                AddOval(600, 150, 1, splitColoring[1]);
                AddOval(300, 450, 2, splitColoring[2]);
                AddOval(450, 450, 3, splitColoring[3]);
                AddOval(150, 800, 4, splitColoring[4]);
                AddOval(600, 800, 5, splitColoring[5]);
                break;
            case 7:
                AddOval(150, 150, 0, splitColoring[0]);
                AddOval(600, 150, 1, splitColoring[1]);
                AddOval(300, 450, 2, splitColoring[2]);
                AddOval(450, 450, 3, splitColoring[3]);
                AddOval(150, 800, 4, splitColoring[4]);
                AddOval(600, 800, 5, splitColoring[5]);
                AddOval(375, 1200, 6, splitColoring[6]);
                break;
            case 8:
                AddOval(150, 150, 0, splitColoring[0]);
                AddOval(600, 150, 1, splitColoring[1]);
                AddOval(300, 450, 2, splitColoring[2]);
                AddOval(450, 450, 3, splitColoring[3]);
                AddOval(150, 800, 4, splitColoring[4]);
                AddOval(600, 800, 5, splitColoring[5]);
                AddOval(300, 1200, 6, splitColoring[6]);
                AddOval(450, 1200, 7, splitColoring[7]);
                break;
        }
    }

    public void AddOval(int xPoint, int yPoint, int ordinal, String colorVertex)
    {
        int numberColor = Integer.parseInt(colorVertex);
        int color = 0;

        switch (numberColor) {
            case 0:
                color = Color.rgb(255, 0, 0);
                break;
            case 1:
                color = Color.rgb(106, 90, 205);
                break;
            case 2:
                color = Color.rgb(46, 139, 87);
                break;
            case 3:
                color = Color.rgb(255, 222, 173);
                break;
            case 4:
                color = Color.rgb(184, 134, 11);
                break;
            case 5:
                color = Color.rgb(47, 79, 79);
                break;
            case 6:
                color = Color.rgb(210, 105, 30);
                break;
            case 7:
                color = Color.rgb(0, 191, 255);
                break;
            default:
                color = Color.rgb(0, 100, 0);
        }

        VertexPoint vertexPoint = new VertexPoint(new Vertex(100, 100, color, ordinal, listEdges, listEdgesHamilton), new Point(xPoint, yPoint));
        vertexList.AddVertexPoint(vertexPoint);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        thread = new MainThread(getHolder(), this);

        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int eventX = (int) event.getX();
        int eventY = (int) event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                for(VertexPoint vertexPoint : vertexList.GetAllVertexPoint())
                {
                    Point point = vertexPoint.getPoint();
                    int radius = Vertex.DIAMETER / 2;
                    if(point.x + radius > eventX && point.x - radius < eventX
                            && point.y + radius > eventY && point.y - radius < eventY)
                    {
                        vertexPoint.getPoint().set(eventX, eventY);
                        break;
                    }
                }
        }

        return true;
        //return super.onTouchEvent(event);
    }

    public void update() {
        for(VertexPoint vertexPoint : vertexList.GetAllVertexPoint())
        {
            vertexPoint.getVertex().update(vertexPoint.getPoint());
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        canvas.drawColor(Color.WHITE);

        for (Edges edges : listEdges.GetAllEdges())
        {
            if(edges.getCount() == 1)
            {
                edges.draw(canvas);
            }
        }

        for(VertexPoint vertexPoint : vertexList.GetAllVertexPoint())
        {
            vertexPoint.getVertex().setTextVisible(textVisible);
            vertexPoint.getVertex().draw(canvas);
        }
    }

    private HamiltonianCycle HamiltonianCycleVertexName(HamiltonianCycle hamiltonianCycle, int count)
    {
        if (count >= 1) {
            hamiltonianCycle.addVertex('0');
        }
        if (count >= 2) {
            hamiltonianCycle.addVertex('1');
        }
        if (count >= 3) {
            hamiltonianCycle.addVertex('2');
        }
        if (count >= 4) {
            hamiltonianCycle.addVertex('3');
        }
        if (count >= 5) {
            hamiltonianCycle.addVertex('4');
        }
        if (count >= 6) {
            hamiltonianCycle.addVertex('5');
        }
        if (count >= 7) {
            hamiltonianCycle.addVertex('6');
        }
        if (count >= 8) {
            hamiltonianCycle.addVertex('7');
        }
        if (count >= 9) {
            hamiltonianCycle.addVertex('8');
        }
        if (count >= 10) {
            hamiltonianCycle.addVertex('9');
        }

        return hamiltonianCycle;
    }
}
