package com.example.czaja.myapplication;

import android.widget.EditText;

/**
 * Created by Czaja on 17.11.2017.
 */

public class ListModel {
    private EditText editText;
    private int vertex;

    public ListModel(EditText editText, int vertex) {
        this.editText = editText;
        this.vertex = vertex;
    }

    public EditText getEditText() {
        return editText;
    }

    public int getVertex() {
        return vertex;
    }
}
