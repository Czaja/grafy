package com.example.czaja.myapplication.fragment;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.czaja.myapplication.R;
import com.example.czaja.myapplication.datebase.Baza;

import java.text.Format;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditFragment extends Fragment {


    private Spinner spinnerMatrixSize;
    private GridLayout gridLayoutMatrix;
    private ArrayList<EditText> listEditText;
    private Button buttonDrawGraph;
    private ArrayList<Integer> listMatrix;
    private Button buttonDrawGraphHamilton;
    private Button buttonDrawGraphEulera;
    private Button buttonDrawGraphBFS;
    private Button buttonDrawGraphMost;
    private Button buttonDrawGraphColoring;

    public EditFragment() {
        // Required empty public constructor
    }

    public EditFragment(ArrayList<Integer> listMatrix) {
        this.listMatrix = listMatrix;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit, container, false);
        spinnerMatrixSize = (Spinner) view.findViewById(R.id.spinner_matrix_size);
        gridLayoutMatrix = (GridLayout) view.findViewById(R.id.draw_matrix);
        buttonDrawGraph = (Button) view.findViewById(R.id.draw_graph);
        buttonDrawGraphHamilton = (Button) view.findViewById(R.id.draw_graph_hamilton);
        buttonDrawGraphEulera = (Button) view.findViewById(R.id.draw_graph_eulera);
        buttonDrawGraphBFS = (Button) view.findViewById(R.id.draw_graph_bfs);
        buttonDrawGraphMost = (Button) view.findViewById(R.id.draw_graph_most);
        buttonDrawGraphColoring = (Button) view.findViewById(R.id.draw_graph_coloring);
        listEditText = new ArrayList<EditText>();

        GenerateMatrix();
        BindButtonDrawGraph();

        return view;
    }

    private void BindButtonDrawGraph()
    {
        buttonDrawGraph.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "edit");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                android.app.Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphHamilton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "edit");
                bundle.putString("algorytm", "cykl_hamiltona");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                android.app.Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphEulera.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "edit");
                bundle.putString("algorytm", "cykl_eulera");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                android.app.Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphBFS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "edit");
                bundle.putString("algorytm", "cykl_bfs");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                android.app.Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphMost.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "edit");
                bundle.putString("algorytm", "cykl_most");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                android.app.Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphColoring.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                for (int i = 0; i < listEditText.size(); i++)
                {
                    listMatrix.add(Integer.parseInt(listEditText.get(i).getText().toString()));
                }
                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "edit");
                bundle.putString("algorytm", "cykl_koloruj");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                android.app.Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });
    }

    private void GenerateMatrix()
    {
        listMatrix = new ArrayList<Integer>();

        Baza baza = new Baza(getActivity());
        String strMatrix = baza.getLastMatrix();
        String[] strlistMatrix;
        strlistMatrix = strMatrix.split(",");
        for ( String strNumber : strlistMatrix)
        {
            listMatrix.add(Integer.valueOf(strNumber));
        }

        gridLayoutMatrix.removeAllViews();
        listEditText = new ArrayList<EditText>();
        int sizeMatrix = (int) Math.sqrt(listMatrix.size());

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(1);
        EditText editTextMatrix = null;

        gridLayoutMatrix.setColumnCount(sizeMatrix);
        gridLayoutMatrix.setRowCount(sizeMatrix);

        for (int i = 0; i < Math.pow(sizeMatrix, 2); i++)
        {
            editTextMatrix = new EditText(this.getActivity());
            editTextMatrix.setFilters(filterArray);
            if (i < listMatrix.size())
            {
                editTextMatrix.setText(String.valueOf(listMatrix.get(i)));
            }
            else
            {
                editTextMatrix.setText("0");
            }

            gridLayoutMatrix.addView(editTextMatrix, i);
            listEditText.add(editTextMatrix);
        }

    }

}
