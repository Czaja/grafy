package com.example.czaja.myapplication.fragment;

import java.io.*;
import java.util.*;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by Czaja on 03.02.2018.
 */

public class Coloring {

    private int V;
    private LinkedList<Integer> adj[];

    public Coloring(int v)
    {
        V = v;
        adj = new LinkedList[v];
        for (int i=0; i<v; ++i)
            adj[i] = new LinkedList();
    }

    public void addEdge(int v,int w)
    {
        adj[v].add(w);
        adj[w].add(v);
    }

    public String greedyColoring()
    {
        int result[] = new int[V];
        String coloring = "";

        Arrays.fill(result, -1);

        result[0]  = 0;

        boolean available[] = new boolean[V];

        Arrays.fill(available, true);

        for (int u = 1; u < V; u++)
        {
            Iterator<Integer> it = adj[u].iterator() ;
            while (it.hasNext())
            {
                int i = it.next();
                if (result[i] != -1)
                    available[result[i]] = false;
            }

            int cr;
            for (cr = 0; cr < V; cr++){
                if (available[cr])
                    break;
            }

            result[u] = cr;

            Arrays.fill(available, true);
        }

        for (int u = 0; u < V; u++)
            coloring += result[u] + ",";

        if (coloring != "")
        {
            coloring = coloring.substring(0, coloring.length() - 1);
        }

        return coloring;
    }
}
