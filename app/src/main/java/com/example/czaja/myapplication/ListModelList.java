package com.example.czaja.myapplication;

import java.util.ArrayList;

/**
 * Created by Czaja on 17.11.2017.
 */

public class ListModelList {
    private ArrayList<ListModel> listModel;

    public ListModelList()
    {
        listModel = new ArrayList<ListModel>();
    }

    public void AddListModel(ListModel listModel)
    {
        this.listModel.add(listModel);
    }

    public ArrayList<ListModel> GetAllIncydencjiModel()
    {
        return listModel;
    };

    public void removeAll()
    {
        listModel = new ArrayList<ListModel>();
    }

    public ListModel getByVertex(int vertex, int edge)
    {
        for (int i = 0; i < GetAllIncydencjiModel().size(); i++)
        {
            if (GetAllIncydencjiModel().get(i).getVertex() == vertex)
            {
                return GetAllIncydencjiModel().get(i);
            }
        }

        return null;
    }

    public ArrayList<ListModel> getByVertex(int vertex)
    {
        ArrayList<ListModel> localListModel = new ArrayList<ListModel>();

        for (int i = 0; i < GetAllIncydencjiModel().size(); i++)
        {
            if (GetAllIncydencjiModel().get(i).getVertex() == vertex && !GetAllIncydencjiModel().get(i).getEditText().getText().toString().equals("-"))
            {
                localListModel.add(GetAllIncydencjiModel().get(i));
            }
        }

        return localListModel;
    }
}
