package com.example.czaja.myapplication.fragment;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.czaja.myapplication.IncydencjiModel;
import com.example.czaja.myapplication.IncydencjiModelList;
import com.example.czaja.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncydecjiFragment extends Fragment {

    private Spinner spinnerMatrixVertices;
    private Spinner spinnerMatrixEdges;
    private GridLayout gridLayoutMatrix;
    private IncydencjiModelList incydencjiModelList;
    private Button buttonDrawGraph;
    private ArrayList<Integer> listMatrix;
    private Button buttonDrawGraphHamilton;
    private Button buttonDrawGraphEulera;
    private Button buttonDrawGraphBFS;
    private Button buttonDrawGraphMost;
    private Button buttonDrawGraphColoring;

    public IncydecjiFragment() {
        // Required empty public constructor
    }

    public IncydecjiFragment(ArrayList<Integer> listMatrix) {
        this.listMatrix = listMatrix;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incydecji, container, false);
        spinnerMatrixVertices = (Spinner) view.findViewById(R.id.spinner_matrix_vertices);
        spinnerMatrixEdges = (Spinner) view.findViewById(R.id.spinner_matrix_edges);
        gridLayoutMatrix = (GridLayout) view.findViewById(R.id.draw_matrix);
        buttonDrawGraph = (Button) view.findViewById(R.id.draw_graph);
        buttonDrawGraphHamilton = (Button) view.findViewById(R.id.draw_graph_hamilton);
        buttonDrawGraphEulera = (Button) view.findViewById(R.id.draw_graph_eulera);
        buttonDrawGraphBFS = (Button) view.findViewById(R.id.draw_graph_bfs);
        buttonDrawGraphMost = (Button) view.findViewById(R.id.draw_graph_most);
        buttonDrawGraphColoring = (Button) view.findViewById(R.id.draw_graph_coloring);
        incydencjiModelList = new IncydencjiModelList();

        BindSpinnerMatrixSize(view);
        BindButtonDrawGraph();

        return view;
    }

    private void BindButtonDrawGraph()
    {
        buttonDrawGraph.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int edges = Integer.parseInt(spinnerMatrixEdges.getSelectedItem().toString());
                int vertices = Integer.parseInt(spinnerMatrixVertices.getSelectedItem().toString());

                if (vertices >= edges)
                {
                    int tab[][] = new int[vertices][vertices];
                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }
                else
                {
                    int tab[][] = new int[edges][edges];
                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "incydencji");
                bundle.putString("algorytm", "");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphHamilton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int edges = Integer.parseInt(spinnerMatrixEdges.getSelectedItem().toString());
                int vertices = Integer.parseInt(spinnerMatrixVertices.getSelectedItem().toString());

                if (vertices >= edges)
                {
                    int tab[][] = new int[vertices][vertices];
                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }
                else
                {
                    int tab[][] = new int[edges][edges];
                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "incydencji");
                bundle.putString("algorytm", "cykl_hamiltona");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphEulera.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int edges = Integer.parseInt(spinnerMatrixEdges.getSelectedItem().toString());
                int vertices = Integer.parseInt(spinnerMatrixVertices.getSelectedItem().toString());

                if (vertices >= edges)
                {
                    int tab[][] = new int[vertices][vertices];
                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }
                else
                {
                    int tab[][] = new int[edges][edges];
                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "incydencji");
                bundle.putString("algorytm", "cykl_eulera");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphBFS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int edges = Integer.parseInt(spinnerMatrixEdges.getSelectedItem().toString());
                int vertices = Integer.parseInt(spinnerMatrixVertices.getSelectedItem().toString());

                if (vertices >= edges)
                {
                    int tab[][] = new int[vertices][vertices];
                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }
                else
                {
                    int tab[][] = new int[edges][edges];
                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "incydencji");
                bundle.putString("algorytm", "cykl_bfs");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphMost.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int edges = Integer.parseInt(spinnerMatrixEdges.getSelectedItem().toString());
                int vertices = Integer.parseInt(spinnerMatrixVertices.getSelectedItem().toString());

                if (vertices >= edges)
                {
                    int tab[][] = new int[vertices][vertices];
                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }
                else
                {
                    int tab[][] = new int[edges][edges];
                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "incydencji");
                bundle.putString("algorytm", "cykl_most");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });

        buttonDrawGraphColoring.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                listMatrix = new ArrayList<Integer>();
                int edges = Integer.parseInt(spinnerMatrixEdges.getSelectedItem().toString());
                int vertices = Integer.parseInt(spinnerMatrixVertices.getSelectedItem().toString());

                if (vertices >= edges)
                {
                    int tab[][] = new int[vertices][vertices];
                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < vertices; i++)
                    {
                        for (int j = 0; j < vertices; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }
                else
                {
                    int tab[][] = new int[edges][edges];
                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            tab[i][j] = 0;
                        }
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        ArrayList<IncydencjiModel> localListIncydencjiModel = incydencjiModelList.getByEdge(i);

                        if (localListIncydencjiModel.size() == 2)
                        {
                            tab[localListIncydencjiModel.get(0).getVertex()][localListIncydencjiModel.get(1).getVertex()] = 1;
                            tab[localListIncydencjiModel.get(1).getVertex()][localListIncydencjiModel.get(0).getVertex()] = 1;
                        }
                    }

                    for (int i = 0; i < edges; i++)
                    {
                        for (int j = 0; j < edges; j++)
                        {
                            listMatrix.add(tab[i][j]);
                        }
                    }
                }

                bundle.putIntegerArrayList("matrix", listMatrix);
                bundle.putString("type", "incydencji");
                bundle.putString("algorytm", "cykl_koloruj");

                LinearLayout linearLayout = (LinearLayout) getActivity().findViewById(R.id.fragment_container);
                linearLayout.removeAllViewsInLayout();

                Fragment fragment = new GraphFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
            }
        });
    }

    private void BindSpinnerMatrixSize(View view) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.matrix_count, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);

        spinnerMatrixVertices.setAdapter(adapter);
        spinnerMatrixEdges.setAdapter(adapter);


        spinnerMatrixVertices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                GenerateMatrix(parentView.getItemAtPosition(position).toString(), spinnerMatrixEdges.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        spinnerMatrixEdges.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                GenerateMatrix(spinnerMatrixVertices.getSelectedItem().toString(), parentView.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
    }

    private void GenerateMatrix(String selectVertices, String selectEdges)
    {
        gridLayoutMatrix.removeAllViews();
        incydencjiModelList.removeAll();

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(1);
        EditText editTextMatrix = null;
        int vertices = Integer.parseInt(selectVertices);
        int edges = Integer.parseInt(selectEdges);
        gridLayoutMatrix.setColumnCount(edges);
        gridLayoutMatrix.setRowCount(vertices);

        int k = 0;
        for (int i = 0; i < vertices; i++)
        {
            for (int j = 0; j < edges; j++)
            {
                editTextMatrix = new EditText(this.getActivity());
                editTextMatrix.setFilters(filterArray);
                editTextMatrix.setText("0");

                gridLayoutMatrix.addView(editTextMatrix, k);
                incydencjiModelList.AddIncydencjiModel(new IncydencjiModel(editTextMatrix, i, j));
                k++;
            }
        }
    }
}
